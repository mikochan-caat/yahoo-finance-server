const { ArgumentParser, RawTextHelpFormatter } = require("argparse");
const package = require("../package.json");
const serverApp = require("../server/app");
const constants = require("../server/constants");
const loggerConfig = require("../server/logger");
const logger = require("../server/logger").default;

loggerConfig.init();
const parser = new ArgumentParser({
    prog: "yfin-tracker",
    description: `${package.description}.\n© ${new Date().getFullYear()} ${package.author}`,
    formatter_class: RawTextHelpFormatter
});

parser.add_argument("-p", "--port", {
    help: `port number to use for the hosted web server (default: ${constants.DefaultPort})`
});
parser.add_argument("-v", "--version", { action: "version", version: package.version });

const parsedArgs = parser.parse_args();
logger.info("Launching...");
logger.info(parser.description);
logger.info();

serverApp.start({ port: parsedArgs.port });
