const fs = require("fs");
const path = require("path");
const util = require("util");
const constants = require("./constants");

const oldLogPath = path.join(constants.PackagePath, "app.old.log");
const logPath = path.join(constants.PackagePath, "app.log");

function tracer() {
    return new Proxy(console, {
        get(target, methodName) {
            const originMethod = target[methodName];

            return function (...args) {
                if (["debug", "trace"].indexOf(methodName.toLowerCase()) === -1) {
                    try {
                        if (fs.statSync(logPath).size >= 50e6) {
                            const log = fs.readFileSync(logPath).toString();
                            fs.writeFileSync(log, log.substring(Math.floor(log.length * 0.5), log.length));
                        }

                        fs.appendFileSync(
                            logPath,
                            `[${methodName.toUpperCase()}] ${new Date().toISOString()}: ${util.format.apply(
                                null,
                                args
                            )}\n`
                        );
                    } catch (ex) {
                        console.error("Logger error!", ex);
                    }
                }

                return originMethod.apply(this, args);
            };
        }
    });
}

exports.init = function () {
    if (fs.existsSync(logPath)) {
        fs.copyFileSync(logPath, oldLogPath);
    }
    fs.writeFileSync(logPath, "");
};

exports.default = tracer();
