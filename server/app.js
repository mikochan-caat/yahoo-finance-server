const fs = require("fs");
const path = require("path");
const process = require("process");
const express = require("express");
const yahoo = require("yahoo-finance2").default;
const package = require("../package.json");
const constants = require("./constants");
const logger = require("./logger").default;

const cachedResponseFile = path.join(constants.PackagePath, "response.cache");

/**
 * @param {Object} options
 * @param {number} [options.port=]
 */
exports.start = function (options) {
    options = options || {};
    options.port = options.port || constants.DefaultPort;

    const app = express();
    app.get("/", (_, res) => res.status(400).send({ message: "Go to /ticker/{stock_id}" }));
    app.get("/ticker", (_, res) => res.status(400).send({ message: "Please specify a stock id." }));
    app.get("/ticker/:id", async (req, res) => {
        if (req.params.id) {
            try {
                const results = await yahoo.quoteSummary(req.params.id);
                const price = results.price;
                const response = {
                    "@type": "Intangible/FinancialQuote",
                    "price": price.regularMarketPrice.toString(),
                    "priceChangePercent": (price.regularMarketChangePercent * 100).toFixed(2)
                };

                fs.writeFileSync(cachedResponseFile, JSON.stringify(response));
                res.send(response);
            } catch (ex) {
                if (ex.code === "ENOTFOUND") {
                    if (fs.existsSync(cachedResponseFile)) {
                        try {
                            const parsed = JSON.parse(fs.readFileSync(cachedResponseFile).toString());
                            parsed.price = `${parsed.price} (cached)`;
                            res.send(parsed);

                            return;
                        } catch {
                            try {
                                fs.unlinkSync(cachedResponseFile);
                            } catch {
                                /* Do nothing */
                            }
                        }
                    }
                }

                logger.error("Can't retrieve ticker info!", ex);
                res.status(500).send({
                    message: "Can't retrieve ticker info!",
                    error: ex.message
                });
            }
        } else {
            res.status(400).send({ message: "Please specify a stock id." });
        }
    });

    const server = app.listen(options.port, constants.HostName, (err) => {
        if (err) {
            logger.error(err);
        } else {
            const indexUrl = `http://${constants.HostName}:${server.address().port}`;
            logger.info("Process ID:", process.pid);
            logger.info(`=== ${package.name} v${package.version} ===`);
            logger.info(`Server running at ${indexUrl}`);
        }
    });
};
