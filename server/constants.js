const path = require("path");

exports.HostName = "localhost";
exports.DefaultPort = 55551;
exports.PackagePath = path.dirname(require.resolve("../package.json"));
